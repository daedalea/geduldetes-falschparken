
## What is this? 
Some simple Webpage, which shows: "Geduldetes Falschparken in Leipzig"

So it lists places where, the city management seams not to enforce parking laws.
This information was collected in a community effort.
 
## Technical Background

Without caring to much, i am plugging a few things here together:

* Hosted via [Gitlab-Pages][gitlabpages]
* Template: plain HTML
* Map: [Leaflef](https://leafletjs.com/) 
* Geojson import tool: 
    - based on this [stackexchange post]( https://gis.stackexchange.com/questions/68489/loading-external-geojson-file-into-leaflet-map)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[gitlabpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html
